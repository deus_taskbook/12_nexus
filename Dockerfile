FROM registry.aixone.tk/repository/registry/python:3.8-slim

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /app

COPY ./requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

#RUN python3 manage.py migrate blog

#RUN ./init.sh

EXPOSE 8000

CMD ["python", "manage.py", "runserver","0.0.0.0:8000"]
